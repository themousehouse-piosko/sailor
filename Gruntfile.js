module.exports = function( grunt ){

	require('load-grunt-tasks')(grunt);
	require('time-grunt')(grunt);

	/**
	 * Default tasks
	 */
	grunt.registerTask('default', [
		'build',
		'serve'
	]);
	grunt.registerTask('build', [
		'jshint',
		'clean:build',
		'concat',
		'html2js',
		'copy',
		'svgpackager',
		'sass'
	]);

	/**
	 * Rapid-Development tasks
	 */
	grunt.registerTask('serve', function( target ){
		grunt.task.run([
			'connect:livereload',
			'watch'
		]);
	});

	// Print a timestamp (useful for when watching)
	grunt.registerTask('timestamp', function(){
		grunt.log.subhead(new Date());
	});

	/**
	 * Grunt configuration
	 */
	grunt.initConfig({
		pkg:        grunt.file.readJSON('package.json'),
		build_dir:  'build',
		assets_dir: '<%= build_dir %>/assets',
		
		
		source_dir: 'src',
		app_dir:    '<%= source_dir %>/app',

		babel: {
			options: {
				sourceMap: true
			},
			dist:    {
				files: {
					"<%= build_dir %>/js/sailor.js": "<%= app_dir %>/sailor.js"
				}
			}
		},

		/**
		 * Template for banner to be automatically pasted into files
		 */
		banner: '/*! <%= pkg.title || pkg.name %> - v<%= grunt.template.today("yyyymmdd") %>\n' +
		' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;\n */\n',

		clean: {
			build:      ['<%= build_dir %>'],
			js:         ['<%= assets_dir %>/js/sailor.js'],
			img:        ['<%= assets_dir %>/img'],
			svgCopy:    ['<%= assets_dir %>/svg'],
			svgPackage: ['<%= assets_dir %>/css/svg.css'],
			vendorJS:   ['<%= assets_dir %>/js/vendor.js']
		},

		/**
		 * Combine multiple files
		 */
		concat: {
			// Main JavaScript files
			js:       {
				options: {
					banner:    '<%= banner %>',
					sourceMap: true
				},
				src:     ['<%= app_dir %>/**/*.js'],
				dest:    '<%= assets_dir %>/js/sailor.js'
			},

			// Build third party plugins
			vendorJS: {
				src:  [
					'vendor/bower_components/angular/angular.min.js',
					'vendor/bower_components/angular-resource/angular-resource.min.js',
					'vendor/bower_components/angular-route/angular-route.min.js',
					'vendor/bower_components/angular-sanitize/angular-sanitize.min.js',
					'vendor/bower_components/lodash/dist/lodash.min.js',
					'vendor/bower_components/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
					'vendor/bower_components/gsap/src/minified/TweenMax.min.js',
					'vendor/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js',
					'vendor/bower_components/gsap/src/minified/plugins/ThrowPropsPlugin.min.js'
				],
				dest: '<%= assets_dir %>/js/vendor.js'
			},
			vendorCSS: {
				src:  [
					'vendor/bower_components/bootstrap/dist/css/bootstrap.min.css'
				],
				dest: '<%= assets_dir %>/css/vendor.css'
			}
		},

		localhost: 'localhost',

		connect: {
			options:    {
				port:       9002,
				livereload: 35729,
				hostname:   '<%= localhost %>'
			},
			livereload: {
				options: {
					open:     true,
					hostname: '<%= localhost %>',
					port:     9002,
					base:     '<%= build_dir %>'
				}
			}
		},

		copy: {
			main: {
				files: [
					{
						expand: true,
						cwd:    '<%= app_dir %>',
						src:    ['index.html'],
						dest:   '<%= build_dir %>/',
						filter: 'isFile'
					}
				]
			},
			fonts: {
				files: [
					{
						expand: true,
						cwd:    '<%= source_dir %>/fonts',
						src:    '**/*',
						dest:   '<%= assets_dir %>/fonts/'
					}
				]
			},
			img:  {
				files: [
					{
						expand: true,
						cwd:    '<%= source_dir %>/img',
						src:    '**/*',
						dest:   '<%= assets_dir %>/img/'
					}
				]
			},
			svg:  {
				files: [
					{
						expand: true,
						cwd:    '<%= source_dir %>/svg/copy',
						src:    '**/*.svg',
						dest:   '<%= assets_dir %>/svg/'
					}
				]
			},
			video:  {
				files: [
					{
						expand: true,
						cwd:    '<%= source_dir %>/video',
						src:    '**/*',
						dest:   '<%= assets_dir %>/video/'
					}
				]
			}
		},

		/**
		 * Convert HTML templates to JavaScript templates for caching
		 */
		html2js: {
			app: {
				options: {
					base:      '<%= app_dir %>/',
					useStrict: true,
					htmlmin:   {
						collapseBooleanAttributes:     true,
						collapseWhitespace:            true,
						removeAttributeQuotes:         true,
						removeComments:                true,
						removeEmptyAttributes:         true,
						removeRedundantAttributes:     true,
						removeScriptTypeAttributes:    true,
						removeStyleLinkTypeAttributes: true
					}
				},
				src:     ['<%= app_dir %>/**/*.tpl.html'],
				dest:    '<%= assets_dir %>/js/templates.js',
				module:  'modules.templates'
			}
		},

		/**
		 * JSHint configuration
		 */
		jshint: {
			files:   [
				'Gruntfile.js',
				'<%= build_dir %>/app/**/*.js'
			],
			options: {
				curly:   true,
				eqeqeq:  true,
				immed:   true,
				latedef: true,
				newcap:  true,
				noarg:   true,
				sub:     true,
				boss:    true,
				eqnull:  true,
				strict:  false,
				node:    true,
				esnext:  true,
				globals: {}
			}
		},

		/**
		 * Compile SASS files
		 */
		sass: {
			dist: {
				options: {
					style: 'expanded'
				},
				files:   [
					{
						expand: true,
						cwd:    '<%= source_dir %>/sass/',
						src:    ['styles.scss'],
						dest:   '<%= assets_dir %>/css',
						ext:    '.css'
					}
				]
			}
		},

		/**
		 * Package SVGs into CSS/JSON
		 */
		svgpackager: {
			svg: {
				options: {
					source:  '<%= source_dir %>/svg/package',
					dest:    '<%= assets_dir %>/css/',
					package: 'svg',
					output:  'css',
					base64:  true
				}
			}
		},

		/**
		 * Watcher configurations - intended for rapid development only
		 */
		watch: {
			main:       {
				files: ['<%= app_dir %>/index.html'],
				tasks: [
					'copy:main'
				]
			},
			html:       {
				files: ['<%= source_dir %>/**/*.tpl.html'],
				tasks: ['html2js']
			},
			js:         {
				files: ['<%= source_dir %>/**/*.js'],
				tasks: [
					'clean:js',
					'jshint',
					'concat:js'
				]
			},
			sass:       {
				files: ['<%= source_dir %>/**/*.scss'],
				tasks: ['sass']
			},
			images:     {
				files: ['<%= source_dir %>/img/**/*'],
				tasks: [
					'clean:img',
					'copy:img',
					'sass'
				]
			},
			svgCopy:    {
				files: ['<% source_dir %>/svg/copy/*.svg'],
				tasks: [
					'clean"svgCopy',
					'copy:svg'
				]
			},
			svgPackage: {
				files: ['<% source_dir %>/svg/package/*.svg'],
				tasks: [
					'clean:svgPackage',
					'svgpackager',
					'sass'
				]
			},
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files:   [
					'<%= watch.main.files %>',
					'<%= watch.html.files %>',
					'<%= watch.js.files %>',
					'<%= watch.sass.files %>',
					'<%= watch.images.files %>',
					'<%= watch.svgCopy.files %>',
					'<%= watch.svgPackage.files %>'
				]
			}
		}

	});

};