(function( angular, undefined ){
	'use strict';

	angular
	.module('Sailor', [
		'ngResource',
		'ngSanitize',
		'modules',
		'sections'
	])
	.controller('AppCtrl', [
		'$scope',
		'$timeout',
		AppController
	]);

	function AppController( $scope, $timeout ){
		$scope.navigation = {
			main: [
				{
					href:     "#about",
					display:  'About',
					cssClass: ''
				},
				{
					href:     "#team",
					display:  'Team',
					cssClass: ''
				},
				{
					href:     "#pricing",
					display:  'Pricing',
					cssClass: ''
				},
				{
					href:     "#careers",
					display:  'Careers',
					cssClass: ''
				},
				{
					href:     "#blog",
					display:  'Blog',
					cssClass: ''
				},
				{
					href:     "#purchase",
					display:  'Purchase',
					cssClass: 'action'
				}
			],
			sub:  [
				{
					href:     "#contact-us",
					display:  'Contact Us',
					cssClass: ''
				},
				{
					href:     "#faq",
					display:  'FAQ',
					cssClass: ''
				},
				{
					href:     "#privacy-policy",
					display:  'Privacy Policy',
					cssClass: ''
				},
				{
					href:     "#terms",
					display:  'Terms',
					cssClass: ''
				}
			]
		};

		$scope.mainMenuActive = false;

		var controller = new ScrollMagic.Controller();
		new ScrollMagic.Scene({triggerElement: "#mast", duration: 400})
			.addTo(controller)
			.on("progress", function (e) {
				$timeout(function(){
					$scope.mainMenuActive = e.progress === 1;
				});
			});
	}

})(window.angular);