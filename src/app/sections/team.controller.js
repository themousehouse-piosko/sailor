/**
 * Created by pioSko on 13.03.2016.
 */

(function( angular, undefined ){
	'use strict';

	angular
	.module('sections.team', [])
	.controller('TeamCtrl', [
		'$scope',
		TeamController
	]);

	function TeamController( $scope ){
		$scope.team = [
			{
				name: 'Paweł Sołyga',
				position: 'Founder',
				img: 'assets/img/team/people-1.png'
			},
			{
				name: 'Konrad Kolasa',
				position: 'Product Designer',
				img: 'assets/img/team/people-2.png'
			},
			{
				name: 'Leon',
				position: 'Mentor',
				img: 'assets/img/team/people-3.png'
			}
		];
	}

})(window.angular);