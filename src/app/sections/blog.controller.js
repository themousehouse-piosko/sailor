/**
 * Created by pioSko on 13.03.2016.
 */

(function( angular, _, undefined ){
	'use strict';

	angular
	.module('sections.blog', [])
	.controller('BlogCtrl', [
		'$scope',
		BlogController
	]);

	function BlogController( $scope ){
		$scope.articles = [
			{
				published: '2 days ago',
				title: 'Drink coffee while designing mobile apps',
				lead: 'Simple tips & tricks, how to make design workflow more effective',
				author: {
					name: 'Konrad Kolasa',
					avatar: 'assets/img/avatars/konradkolasa.jpg'
				},
				image: "assets/img/Picture1.jpg",
				blocks: 2
			},
			{
				published: '2 days ago',
				title: 'Start from paper sketches',
				lead: 'Start thinking of design process with pencil, paper and head full of ideas',
				author: {
					name: 'Konrad Kolasa',
					avatar: 'assets/img/avatars/konradkolasa.jpg'
				},
				image: "assets/img/Picture2.jpg",
				blocks: 1
			}
		];

		_.each($scope.articles, function(article){
			_.assign(article, {cssClass: 'col-sm-' + (article.blocks * 4)});
		});
	}

})(window.angular, window._);