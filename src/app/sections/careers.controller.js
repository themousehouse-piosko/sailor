/**
 * Created by pioSko on 13.03.2016.
 */

(function( angular, undefined ){
	'use strict';

	angular
	.module('sections.careers', [])
	.controller('CareersCtrl', [
		'$scope',
		CareersController
	]);

	function CareersController( $scope ){
		$scope.jobs = [
			{
				position: 'Product Manager',
				description: 'quis volutpat ipsum. Aenean iaculis magna nex ex posuere feugiat. Proin pretium lacus pharetra orci viverra consequat',
				location: 'Wroclaw'
			},
			{
				position: 'Senior Front-End Developer',
				description: 'quis volutpat ipsum. Aenean iaculis magna nex ex posuere feugiat. Proin pretium lacus pharetra orci viverra consequat',
				location: 'Wroclaw'
			},
			{
				position: 'UI/UX Designer',
				description: 'quis volutpat ipsum. Aenean iaculis magna nex ex posuere feugiat. Proin pretium lacus pharetra orci viverra consequat',
				location: 'Wroclaw'
			}
		];
	}

})(window.angular);