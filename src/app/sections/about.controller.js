/**
 * Created by pioSko on 13.03.2016.
 */

(function( angular, undefined ){
	'use strict';

	angular
	.module('sections.about', [])
	.controller('AboutCtrl', [
		'$scope',
		AboutController
	]);

	function AboutController( $scope ){
		$scope.perks = [
			{
				title: 'Clean &amp; Simple',
				lead: 'Simple and clean designed website is a milestone to achieve success.',
				icon: 'perk_icon_1'
			},
			{
				title: 'Pixel perfection',
				lead: 'Every pixel has not accidentialy place and color to keep the design perfect.',
				icon: 'perk_icon_2'
			},
			{
				title: 'Retina ready',
				lead: 'This website is designed to look the same incredible on Retina displays.',
				icon: 'perk_icon_3'
			},
			{
				title: 'Responsive',
				lead: 'We think of all your users that will experience this website on mobile.',
				icon: 'perk_icon_4'
			},
			{
				title: 'Sketch',
				lead: 'We use Sketch as a simplest way to have fully-editable vectors',
				icon: 'perk_icon_5'
			}
		];
	}

})(window.angular);