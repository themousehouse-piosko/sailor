(function(angular, undefined){
	'use strict';

	angular.module('sections', [
		'sections.about',
		'sections.team',
		'sections.careers',
		'sections.blog'
	])

})(window.angular);