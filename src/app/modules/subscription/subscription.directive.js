(function( angular, undefined ){
	'use strict';

	angular
	.module('modules')
	.directive('subscription', [SubscriptionDirective]);

	function SubscriptionDirective(){
		return {
			restrict:    'EA',
			templateUrl: 'modules/subscription/subscription.tpl.html',
			link:        link,
			scope:       {
				source: '=',
				id:     '@'
			}
		};

		function link( scope ){
			scope.id = scope.id || ('nav_$' + scope.$id);
			scope.pattern = {
				email: /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
			};

			scope.userEmail = '';

			scope.submit = function(event, form){
				event.preventDefault();
				if(form.email.$valid){
					console.log(scope.userEmail, form);
				}
			}
		}
	}

})(window.angular);