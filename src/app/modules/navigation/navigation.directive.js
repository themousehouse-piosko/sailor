(function( angular, undefined ){
	'use strict';

	angular
	.module('modules')
	.directive('navigation', [NavDirective]);

	function NavDirective(){
		return {
			restrict:    'EA',
			templateUrl: 'modules/navigation/navigation.tpl.html',
			transclude:  true,
			link:        link,
			scope:       {
				source: '=',
				id:     '@'
			}
		};

		function link( scope ){
			scope.id = scope.id || ('nav_$' + scope.$id);
		}
	}

})(window.angular);